import os
import math
import random

#init variables
p = 0 #probability of the engineered bias manifesting
u = 0 #random var used to determine which bias manifests in conjunction with p
v = 0 #a byte generated and potentially appended to the sequence
c = 0 # counter
fname = "hybrid"

for a in range (0,101,10):
	filename = fname+str(a)
	
	for i in range(0,101,10):
		p = i/100
		
		for j in range(0,101,10):
			f = open(filename+"_p"+str(i)+"es"+str(j)+".bin",'wb')	
			q = j/100
			
			print(str(filename)+" p = "+str(i)+" es = "+str(j))
			
			for k in range(0,10000000):
				u = random.uniform(0.00,1.00)
				 
				if (u <= p):
					es = random.uniform(0.00,1.00)
					
					if (es <= q):
						v = c
						#print(c)
						c = (c+1)%256			
					else:
						temp = int.from_bytes(os.urandom(1),byteorder='little')
						while temp == 255:
							temp = int.from_bytes(os.urandom(1),byteorder='little')
						v = temp
					
				else:
					v = int.from_bytes(os.urandom(1),byteorder='little')	
					
				f.write(bytes([v]))
			
			f.close()
	
