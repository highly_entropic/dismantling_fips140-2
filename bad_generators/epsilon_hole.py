import os
import math
import random

#init variables
p = 0 #probability of the switch flipping
urand = 0 #random var used to determine whether switch flips in conjunction with p
value = 0 #a byte generated and potentially appended to the sequence
fname = "epsilon/epsilon"

for a in range (0,10):
	filename = fname+str(a)
	print(filename)
	
	for i in range(1,101):
		p = i/100
		print(p)
		f = open(filename+"_"+str(i)+".bin",'wb')	
		for j in range(0,10000000):
			urand = random.uniform(0.00,1.00)
                        
			if (urand <= p):
				temp = int.from_bytes(os.urandom(1),byteorder='little')
				while temp == 255:
					temp = int.from_bytes(os.urandom(1),byteorder='little')
				value = temp
			else:
				value = int.from_bytes(os.urandom(1),byteorder='little')
				
			f.write(bytes([value]))
		f.close()
	
