import os
import math
import random
import time
import csv

def shift(seq, n):
    n = n % len(seq)
    return seq[n:] + seq[:n]

perms = []
prev_value = 0

with open('permutation_file.csv','r') as csvfile: 
# read the permutations csv in and load rows into memory for use in further tests
	csv_reader = csv.reader(csvfile,delimiter=',')
	for row in csv_reader:
		perms.append([int(x) for x in row])
		
#init variables
p = 0 #probability of the switch flipping
urand = 0 #random var used to determine whether switch flipsin conjunction with p

for a in range (0,10):
	filename = 'shift-sigma'+str(a)
	print(filename)

	for i in range(100,101): # set range of probabilities
		start=time.time()
		p = i/100
		print(p)
		f = open(filename+"_"+str(i)+".bin",'wb')
		#count = 0
		prev_value = 0
		perm = perms[0]
		
		for j in range(0,15000000):
			urand = random.uniform(0.00,1.00)
			
			if (urand <= p):
				value = perm[prev_value] # initialised to position 0, but takes the value of each successive byte (after shift)
				#print(len(perm))
				perm = shift(perm,1) # shift permutation by 1 to left
				prev_value = value
			else:
				value = int.from_bytes(os.urandom(1),byteorder='little')	
				
			f.write(bytes([value]))
		stop=time.time()
		print(stop-start)
		f.close()
		