import os
import math
import random
import time
import csv

perms = []
prev_value = 0

with open('permutation_file.csv','r') as csvfile:
	csv_reader = csv.reader(csvfile,delimiter=',')
	for row in csv_reader:
		perms.append([int(x) for x in row])
		
#init variables
p = 0 #probability of the switch flipping
urand = 0 #random var used to determine whether switch flipsin conjunction with p

for a in range (0,10):
	filename = 'imp-sigma/value-based-1024/imp-sigma'+str(a)
	print(filename)

	for i in range(10,21):
		start=time.time()
		p = i/100
		print(p)
		f = open(filename+"_"+str(i)+".bin",'wb')
		#count = 0
		prev_value = 0

		for j in range(0,50000000):
			urand = random.uniform(0.00,1.00)

			if (urand <= p):
				#value = perms[random.randint(0,3)][count] #uncomment for counter based permutation iteration
				bias_value = perms[random.randint(0,1023)][prev_value] #uncomment for value based permutation iteration - max val must be upper range -1 of initialising at 0
				#bias_value = perms[int.from_bytes(os.urandom(1),byteorder='little')][prev_value] #uncomment for value based permutation iteration
				prev_value = bias_value
				value = bias_value
				#count = (count+1)%256
			else:
				value = int.from_bytes(os.urandom(1),byteorder='little')	
				
			f.write(bytes([value]))
		stop=time.time()
		print(stop-start)
		f.close()
		
