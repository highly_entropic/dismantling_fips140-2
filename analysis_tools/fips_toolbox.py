import subprocess
import csv
import matplotlib.pyplot as plt
import numpy as np
import string
from scipy import stats

bits = []
passed = []
failed = []
mono = []
poker = []
runs = []
long_run = []
cont_run = []
xax = []

r_bits = []
r_passed = []
r_failed = []
r_mono = []
r_poker = []
r_runs = []
r_long_run = []
r_cont_run = []


#print('Running FIPS 140-2 Tests over epsilon hole data')
#subprocess.call('./fipsscript')

for i in range (0,10):
	print('Analysing data for iteration ' + str(i))
	temp_ld = []
	bits.append([])
	passed.append([])
	failed.append([])
	mono.append([])
	poker.append([])
	runs.append([])
	long_run.append([])
	cont_run.append([])
	
	with open('fips/random/fips_urandom_1k_'+str(i)+'.csv','r') as k:
			rows = list(csv.reader(k,delimiter=' '))
			r_bits.append(rows[5][5])
			r_passed.append(rows[6][4])
			r_failed.append(rows[7][4])
			r_mono.append(rows[8][4])
			r_poker.append(rows[9][4])
			r_runs.append(rows[10][4])
			r_long_run.append(rows[11][5])
			r_cont_run.append(rows[12][5])
	
	for j in range (1,100):
		#with open('fips/epsilon/fips_epsilon'+str(i)+'_'+str(j)+'.csv','r') as f:
		with open('fips/imp-sigma/counter-like/fips1402-imp-sigma'+str(i)+'_'+str(j)+'.csv','r') as f:
			rows = list(csv.reader(f,delimiter=' '))
			bits[i].append(rows[5][5])
			passed[i].append(rows[6][4])
			failed[i].append(rows[7][4])
			mono[i].append(rows[8][4])
			poker[i].append(rows[9][4])
			runs[i].append(rows[10][4])
			long_run[i].append(rows[11][5])
			cont_run[i].append(rows[12][5])

for i in range (1,100):
	xax.append(float(int(i)/100.00))
	
k.close()
f.close()

passed = np.array(passed).astype(np.int)
failed = np.array(failed).astype(np.int)
mono = np.array(mono).astype(np.int)
poker = np.array(poker).astype(np.int)
runs = np.array(runs).astype(np.int)
long_run = np.array(long_run).astype(np.int)
cont_run = np.array(cont_run).astype(np.int)

print(failed.max())
print(mono.max())
print(poker.max())
print(runs.max())
print(long_run.max())
print(cont_run.max())


r_passed = np.array(r_passed).astype(np.int)
r_failed = np.array(r_failed).astype(np.int)
r_mono = np.array(r_mono).astype(np.int)
r_poker = np.array(r_poker).astype(np.int)
r_runs = np.array(r_runs).astype(np.int)
r_long_run = np.array(r_long_run).astype(np.int)
r_cont_run = np.array(r_cont_run).astype(np.int)

mean_pass = np.mean(passed,axis=0)
mean_fail = np.mean(failed,axis=0)
mean_mono = np.mean(mono,axis=0)
mean_poker = np.mean(poker,axis=0)
mean_runs = np.mean(runs,axis=0)
mean_lr = np.mean(long_run,axis=0)
mean_cr = np.mean(cont_run,axis=0)

mean_r_pass = np.mean(r_passed)
mean_r_fail = np.mean(r_failed)
mean_r_mono = np.mean(r_mono)
mean_r_poker = np.mean(r_poker)
mean_r_runs = np.mean(r_runs)
mean_r_lr = np.mean(r_long_run)
mean_r_cr = np.mean(r_cont_run)

print(mean_r_pass)

passed_dev = np.std(passed,axis=0)/2
failed_dev = np.std(failed,axis=0)/2
mono_dev = np.std(mono,axis=0)/2
poker_dev = np.std(poker,axis=0)/2
runs_dev = np.std(runs,axis=0)/2
lr_dev = np.std(long_run,axis=0)/2
cr_dev = np.std(cont_run,axis=0)/2

passed_r_dev = np.std(r_passed)/2
failed_r_dev = np.std(r_failed,axis=0)/2
mono_r_dev = np.std(r_mono,axis=0)/2
poker_r_dev = np.std(r_poker,axis=0)/2
runs_r_dev = np.std(r_runs,axis=0)/2
lr_r_dev = np.std(r_long_run,axis=0)/2
cr_r_dev = np.std(r_cont_run,axis=0)/2

plt.rcParams.update({'font.size': 10})

i = 1
j = 100

fig,ax1 = plt.subplots(1,figsize=(4,3))
plt.gca().set_xlim(left=i/100)
plt.gca().set_xlim(right=j/100)
ax1.errorbar(xax[i:j],mean_pass[i:j], passed_dev[i:j], fmt='ok', markersize=1, lw=1, capsize=1)
plt.fill_between(xax,mean_r_pass-passed_r_dev,mean_r_pass+passed_r_dev,facecolor='red',alpha=0.5)
#plt.axhline(990)
ax1.set_xlabel('sigma')
ax1.set_ylabel('Passed')
#plt.title('FIPS140-2 Pass rate (aggregated results of rngtest utility)')
plt.tight_layout()
plt.grid(True)

plt.figure(2,figsize=(4,3))
#plt.subplot(3,1,2)
plt.gca().set_xlim(left=i/100)
plt.gca().set_xlim(right=j/100)
plt.errorbar(xax[i:j],mean_mono[i:j], mono_dev[i:j],ecolor='red', fmt='or', markersize=1, lw=1, capsize=1,label='monobits')
plt.fill_between(xax,mean_r_mono-mono_r_dev,mean_r_mono+mono_r_dev,facecolor='red',alpha=0.5)
plt.xlabel('sigma')
plt.ylabel('failed tests')
plt.errorbar(xax[i:j],mean_poker[i:j], poker_dev[i:j],ecolor='green', fmt='og', markersize=1, lw=1, capsize=1,label='poker')
plt.fill_between(xax,mean_r_poker-poker_r_dev,mean_r_poker+poker_r_dev,facecolor='green',alpha=0.5)
plt.errorbar(xax[i:j],mean_runs[i:j], runs_dev[i:j],ecolor='blue', fmt='ob', markersize=1, lw=1, capsize=1,label='runs')
plt.fill_between(xax,mean_r_runs-runs_r_dev,mean_r_runs+runs_r_dev,facecolor='blue',alpha=0.5)
plt.errorbar(xax[i:j],mean_lr[i:j], lr_dev[i:j],ecolor='cyan', fmt='oc', markersize=1, lw=1, capsize=1,label='long run')
plt.fill_between(xax,mean_r_lr-lr_r_dev,mean_r_lr+lr_r_dev,facecolor='cyan',alpha=0.5)
plt.errorbar(xax[i:j],mean_cr[i:j], cr_dev[i:j],ecolor='magenta', fmt='om', markersize=1, lw=1, capsize=1, label='continuous run')
plt.fill_between(xax,mean_r_cr-cr_r_dev,mean_r_cr+cr_r_dev,facecolor='magenta',alpha=0.5)
plt.ylim(0)
#plt.title('Failure rates for all FIPS140-2 Tests (rngtest utility)')
plt.tight_layout()
plt.legend()
plt.grid(True)

plt.show()
