import os
from bitstring import BitArray
from PIL import Image

f = open("imp-sigma/t-counter/window-4_8/t-counter0_100.bin","rb")
b = BitArray(f.read(10000))

# PIL accesses images in Cartesian co-ordinates, so it is Image[columns, rows]
img = Image.new( 'RGB', (150,200), "black") # create a new black image
pixels = img.load() # create the pixel map

for i in range(img.size[0]):    # for every col:
	d = b[i*img.size[1]:(i*img.size[1])+img.size[1]]
	
	for j in range(img.size[1]):    # For every row
		#print(j)
		if d[j] == True:
			pixels[i,j] = (0,0,0) # set the colour accordingly
		else:
			pixels[i,j] = (255,255,255) # set the colour accordingly

img.save("graphs/t-counter_win-4_8_0_100.png","PNG")
img.show()