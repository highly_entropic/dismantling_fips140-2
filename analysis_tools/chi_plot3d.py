import csv
import subprocess
import sys
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
from scipy import stats, io

xax = []

chi_list = np.zeros((11,11,11))
ent_list = np.zeros((11,11,11))
corr_list = np.zeros((11,11,11))
bin_abs_255 = np.zeros((11,11,11))
bin_prop_255 = np.zeros((11,11,11))

for k in range (0,101,10):
	filename = 'ent/hybrid/ent_hybrid'+str(k)+'_'
	print(filename)
	
	for i in range (0,101,10):
		for j in range(0,101,10):
			with open(filename+"p"+str(i)+"es"+str(j)+'.csv','r') as f:	
				rows = list(csv.reader(f))
				chi_list[k//10,i//10,j//10] = float(rows[1][3])
				corr_list[k//10,i//10,j//10] = float(rows[1][6])
				ent_list[k//10,i//10,j//10] = float(rows[1][2])
				bin_abs_255[k//10,i//10,j//10] = float(rows[258][2])
				bin_prop_255[k//10,i//10,j//10] = float(rows[258][3])
				
f.close()

io.savemat('testmatimport.mat',mdict={'arr':ent_list})

for i in range (0,100,10):
	xax.append(float(int(i)/100.00))

chi_mean = np.mean(chi_list,axis=0)
chi_mean_p = 1-stats.chi2.cdf(chi_mean,255)

corr_mean = np.mean(corr_list,axis=0)
ent_mean = np.mean(ent_list,axis=0)
bin_prop255_mean = np.mean(bin_prop_255,axis=0)

x = []
y = []
for i in range (0,11):
	if i == 0:
		x.append(0)
		y.append(0)
	else:
		x.append(i/10)
		y.append(i/10)

X,Y = np.meshgrid(x,y)

plt.rcParams.update({'font.size': 10})

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.set_xlabel('es')
ax.set_ylabel('p')
ax.set_zlabel('chi score')

ax.plot_surface(X,Y,chi_mean)

fig2 = plt.figure()
ax2 = fig2.add_subplot(111, projection='3d')
ax2.set_xlabel('es')
ax2.set_ylabel('p')
ax2.set_zlabel('correlation')

ax2.plot_surface(X,Y,corr_mean)

fig3 = plt.figure()
ax3 = fig3.add_subplot(111, projection='3d')
ax3.set_xlabel('es')
ax3.set_ylabel('p')
ax3.set_zlabel('entropy')

ax3.plot_surface(X,Y,ent_mean)

plt.show()
