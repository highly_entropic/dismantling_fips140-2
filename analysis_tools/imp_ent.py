import os, sys, getopt
from improved_minentropy import improved_entropy
import pprint
import csv
import time


def main(argv):
	inputfile = []
	
	# select input dir and output file
	try:
		opts, args = getopt.getopt(argv,"hi:o",["idir=","ofile="])
	
	except getopt.GetoptError:
		print('test.py -i <inputdir> -o <output file>')
		sys.exit(2)
	
	for opt, arg in opts:
		if opt == '-h':
			print('test.py -i <inputdir> -o <output file>')
			sys.exit()
		elif opt in ("-i", "--idir"):
			inputdir = arg
		elif opt in ("-o", "--ofile"):
			outputfile = args
		
	# populate a list of filenames		
	for (dirpath, dirnames, filenames) in os.walk(inputdir):		
		inputfile = filenames
	
	# for every file in filenames, check whether the booltest is passed or failed 	
	with open(args[0]+'.csv','w') as csvfile:
		csvwriter = csv.writer(csvfile,delimiter = ',', quotechar = '|', quoting=csv.QUOTE_MINIMAL)
		csvwriter.writerow(['Filename', 'w', 'p', 'p 0.99', 'min entropy'])
		
		for file in inputfile:
			if file[-4:] == '.bin':		
				print(inputdir+file)
				start = time.time()				
				w,p,p99,minent = improved_entropy(inputdir+file)	
				end = time.time()
				print(end - start)
				csvwriter.writerow([file, w, p, p99, minent])									
	
if __name__ == "__main__":
	main(sys.argv[1::])