import csv
import subprocess
import sys
import matplotlib.pyplot as plt
import numpy as np
from scipy import stats



chi_list = []
corr_list = []
ent_list = []
bin_abs_255 = []
bin_prop_255 = []
xax = []

chi_mean = []
corr_mean = []
ent_mean = []

r_chi_list = []
r_corr_list = []
r_ent_list = []

#print('Running Ent Tests over sigma counter and epsilon hole data')
#subprocess.call('./entscript')

for j in range (0,10):
	chi_list.append([])
	corr_list.append([])
	ent_list.append([])
	bin_abs_255.append([])
	bin_prop_255.append([])
	
	with open('ent/random/ent_urandom'+str(j)+'.csv','r') as r:	
			rows = list(csv.reader(r))	
			r_chi_list.append(float(rows[1][3]))
			r_corr_list.append(float(rows[1][6]))
			r_ent_list.append(float(rows[1][2]))
		
	#filename = 'ent/epsilon/ent_testepsilon'+str(j)+'_'
	filename = 'ent/imp-sigma/value-based/ent-imp-sigma'+str(j)+'_'
	print(filename)
	
	for i in range (1,101):
		with open(filename+str(i)+'.csv','r') as f:	
			rows = list(csv.reader(f))	
			chi_list[j].append(float(rows[1][3]))
			corr_list[j].append(float(rows[1][6]))
			ent_list[j].append(float(rows[1][2]))
			#print(rows[258])
			bin_abs_255[j].append(float(rows[258][2]))
			bin_prop_255[j].append(float(rows[258][3]))

r.close()
f.close()

for i in range (1,101):
	xax.append(float(int(i)/100.00))

chi_list = np.array(chi_list)
corr_list = np.array(corr_list)
ent_list = np.array(ent_list)
bin_prop255_list = np.array(bin_prop_255)

r_chi_list = np.array(r_chi_list)
r_corr_list = np.array(r_corr_list)
r_ent_list = np.array(r_ent_list)

chi_mean = np.mean(chi_list,axis=0)
chi_mean_p = 1-stats.chi2.cdf(chi_mean,255)
#print(chi_mean_p)

r_chi_mean = np.mean(r_chi_list)

corr_mean = np.mean(corr_list,axis=0)
ent_mean = np.mean(ent_list,axis=0)

r_corr_mean = np.mean(r_corr_list,axis=0)
r_ent_mean = np.mean(r_ent_list,axis=0)

bin_prop255_mean = np.mean(bin_prop255_list,axis=0)

chi_dev = np.std(chi_list,axis=0)/2
corr_dev = np.std(corr_list,axis=0)/2
ent_dev = np.std(ent_list,axis=0)/2

r_chi_dev = np.std(r_chi_list)/2
r_corr_dev = np.std(r_corr_list)/2
r_ent_dev = np.std(r_ent_list)/2

bin_prop255_dev = np.std(bin_prop255_list,axis=0)/2

plt.rcParams.update({'font.size': 10})

fig,ax1 = plt.subplots(1,figsize=(4,3))
ax1.errorbar(xax,chi_mean, chi_dev, fmt='ok', markersize= 1, lw=1, capsize=1, label='chi-score')
ax1.fill_between(xax,r_chi_mean-r_chi_dev,r_chi_mean+r_chi_dev,facecolor='red',alpha=0.5)
#ax1.set_yscale("log",nonposy='clip')
ax1.set_xlabel('sigma')
ax1.set_ylabel('score')

ax2=ax1.twinx()
ax2.plot(xax,chi_mean_p,label='p-value')
ax2.set_ylabel('p-value')
ax1.grid(True)
plt.title('Chi-sq. Score and p-value')
fig.tight_layout()

plt.figure(2,figsize=(4,3))
#plt.subplot(3,1,2)
plt.errorbar(xax,corr_mean, corr_dev, fmt='ok', markersize= 1, lw=1, capsize=1)
plt.fill_between(xax,r_corr_mean-r_corr_dev,r_corr_mean+r_corr_dev,facecolor='red',alpha=0.5)
plt.xlabel('sigma')
plt.ylabel('serial correlation')
#plt.yscale('log')
plt.title('Serial Correlation')
plt.tight_layout()
plt.grid(True)

plt.figure(3,figsize=(4,3))
#plt.subplot(3,1,3)
plt.errorbar(xax,ent_mean, ent_dev, fmt='ok', markersize= 1, lw=1, capsize=1)
plt.fill_between(xax,r_ent_mean-r_ent_dev,r_ent_mean+r_ent_dev,facecolor='red',alpha=0.5)
plt.xlabel('sigma')
plt.ylabel('entropy')
#plt.yscale('log')
plt.title('Estimated Entropy')
plt.tight_layout()
plt.grid(True)

plt.figure(4,figsize=(4,3))
#plt.subplot(3,1,3)
plt.errorbar(xax,bin_prop255_mean, bin_prop255_dev, fmt='ok', markersize= 1, lw=1, capsize=1)
#plt.plot(bin_abs255_mean)
plt.xlabel('epsilon')
plt.ylabel('Occurrences')
#plt.yscale('log')
plt.title('Log-scale proportion of bin 255 in all samples')
plt.tight_layout()
plt.grid(True)

plt.show()
