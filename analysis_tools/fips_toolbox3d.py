import csv
import subprocess
import sys
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
from scipy import stats, io

bits = np.zeros((11,11,11))
passed = np.zeros((11,11,11))
failed = np.zeros((11,11,11))
mono = np.zeros((11,11,11))
poker = np.zeros((11,11,11))
runs = np.zeros((11,11,11))
long_run = np.zeros((11,11,11))
cont_run = np.zeros((11,11,11))

xax = []

for k in range (0,101,10):
	filename = 'fips/hybrid/hybrid'+str(k)+'_'
	print(filename)
	
	for i in range (0,101,10):
		for j in range(0,101,10):
			with open(filename+"p"+str(i)+"es"+str(j)+'.csv','r') as f:	
				rows = list(csv.reader(f,delimiter=' '))	
				#print(rows[6][4])
				#bits[k//10,i//10,j//10] = rows[6][4]
				passed[k//10,i//10,j//10] = rows[6][4]
				failed[k//10,i//10,j//10] = rows[7][4]
				mono[k//10,i//10,j//10] = rows[8][4]
				poker[k//10,i//10,j//10] = rows[9][4]
				runs[k//10,i//10,j//10] = rows[10][4]
				long_run[k//10,i//10,j//10] = rows[11][5]
				cont_run[k//10,i//10,j//10] = rows[12][5]
				
f.close()

#io.savemat('testmatimport.mat',mdict={'arr':ent_list})

for i in range (0,100,10):
	xax.append(float(int(i)/100.00))

bits_mean = np.mean(bits,axis=0)
passed_mean = np.mean(passed,axis=0)
failed_mean = np.mean(failed,axis=0)
mono_mean = np.mean(mono,axis=0)
poker_mean = np.mean(poker,axis=0)
runs_mean = np.mean(runs,axis=0)
long_mean = np.mean(long_run,axis=0)
cont_mean = np.mean(cont_run,axis=0)

nx, ny = 11, 11

x = range(nx)
y = range(ny)

X,Y = np.meshgrid(x,y)

plt.rcParams.update({'font.size': 10})

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.set_xlabel('es')
ax.set_ylabel('p')
ax.set_zlabel('FIPS tests passed')

ax.plot_surface(X,Y,passed_mean)

fig2 = plt.figure()
ax2 = fig2.add_subplot(111, projection='3d')
ax2.set_xlabel('es')
ax2.set_ylabel('p')
ax2.set_zlabel('FIPS individual tests failed')

ax2.plot_surface(X,Y,poker_mean,label='poker')
ax2.plot_surface(X,Y,mono_mean,label='mono')
ax2.plot_surface(X,Y,runs_mean,label='run')
ax2.plot_surface(X,Y,long_mean,label='long run')
ax2.plot_surface(X,Y,cont_mean,label='continuous run')

#ax2.legend(loc='upper left')

plt.show()
