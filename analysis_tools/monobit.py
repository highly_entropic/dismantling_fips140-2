import sys
import math
from scipy import special as spc

def monobit(in_f):
	with open(in_f,'rb') as file:
		bin_data = file.read()
	
	count = 0
	ones = 0
	zeroes = 0
    # If the char is 0 minus 1, else add 1
	for char in bin_data:
		b_str = bin(char)
		
		for i in b_str[2:]:
			if i == '0':
				count -= 1
				zeroes += 1
			else:
				count += 1
				ones += 1
				
    # Calculate the p value
	sobs = count / math.sqrt(len(bin_data))
	p_val = spc.erfc(math.fabs(sobs) / math.sqrt(2))
	print(count)
	print(ones+zeroes)
	print(p_val)
	
	return p_val

if __name__ == '__main__':
    # Map command line arguments to function arguments.
    monobit(str(sys.argv[1]))